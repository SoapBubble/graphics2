﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;

namespace Graphics2
{
    public partial class Form1 : Form
    {
        private Image OriginalImage;

        public Form1()
        {
            InitializeComponent();
        }

        private void loadImageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog d = new OpenFileDialog();
            d.Filter = "Image Files|*.BMP;*.GIF;*.JPG;*.JPEG;*.PNG|All files (*.*)|*.*";
            var res = d.ShowDialog();
            if (res == DialogResult.OK)
            {
                OriginalImage = Image.FromFile(d.FileName);
                pictureBox1.Image = OriginalImage;
                RGB();
            }
        }

        private Color RgbFromHue(int hue)
        {
            var h = (hue/60)%6;
            var a = 100*(hue%60)/60;
            int Vinc = a*255/100, Vdec = (100 - a)*255/100, V = 255, Vmin = 0;
            switch (h)
            {
                case 0:
                    return Color.FromArgb(V, Vinc, Vmin);
                case 1:
                    return Color.FromArgb(Vdec, V, Vmin);
                case 2:
                    return Color.FromArgb(Vmin, V, Vinc);
                case 3:
                    return Color.FromArgb(Vmin, Vdec, V);
                case 4:
                    return Color.FromArgb(Vinc, Vmin, V);
                case 5:
                    return Color.FromArgb(V, Vmin, Vdec);
            }
            return Color.Black;
        }

        int getH(Color c)
        {
            var max = new[] {c.R, c.B, c.G}.Max();
            var min = new[] {c.R, c.B, c.G}.Min();
            if (max == min)
                return 0;
            if (max == c.R && c.G >= c.B)
                return 60*(c.G - c.B)/(max - min);
            if (max == c.R && c.G < c.B)
                return 60*(c.G - c.B)/(max - min) + 360;
            if (max == c.G)
                return 60*(c.B - c.R)/(max - min) + 120;
            return 60*(c.R - c.G)/(max - min) + 240;
        }

        private int getS(Color c)
        {
            var max = new[] {c.R, c.B, c.G}.Max();
            var min = new[] {c.R, c.B, c.G}.Min();
            if (max == 0)
                return 0;
            return 255 - min*255/max;
        }


        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetColorSpace();
        }

        private void GetColorSpace()
        {
            var s = comboBox1.SelectedItem as string;
            switch (s)
            {
                case "RGB":
                    RGB();
                    break;
                case "HSV":
                    HSV();
                    break;
                case "Lab":
                    Lab();
                    break;
                default:
                    YcbCr();
                    break;
            }
        }

        double[] XyzToLab(Color c, Matrix<double> m, Vector<double> white)
        {
            var xyz = m*new DenseVector(new double[] {c.R/255.0, c.G/255.0, c.B/255.0});
            double X = xyz[0], Y = xyz[1], Z = xyz[2], k=903.3;
            
            double var_X, var_Y, var_Z;
            var_X = X/white[0];
            var_Y = Y/white[1];
            var_Z = Z/white[2];
            if (var_X > 0.008856) var_X = Math.Pow(var_X, (1.0/3));
            else var_X = (k*var_X + 16)/116;
            if (var_Y > 0.008856) var_Y = Math.Pow(var_Y, (1.0/3));
            else var_Y = (k * var_Y + 16) / 116;
            if (var_Z > 0.008856) var_Z = Math.Pow(var_Z, (1.0/3));
            else var_Z = (k * var_Z + 16) / 116;
            return new[] {(116*var_Y) - 16, 500*(var_X - var_Y), 200*(var_Y - var_Z)};

        }


        double GetVal(TextBox tb)
        {
            return GetDouble(tb.Text, 0);

        }

        public static double GetDouble(string value, double defaultValue)
        {
            double result;

            //Try parsing in the current culture
            if (!double.TryParse(value, System.Globalization.NumberStyles.Any, CultureInfo.CurrentCulture, out result) &&
                //Then try in US english
                !double.TryParse(value, System.Globalization.NumberStyles.Any, CultureInfo.GetCultureInfo("en-US"), out result) &&
                //Then in neutral language
                !double.TryParse(value, System.Globalization.NumberStyles.Any, CultureInfo.InvariantCulture, out result))
            {
                result = defaultValue;
            }

            return result;
        }

        private void YcbCr()
        {
            label1.Text = "Y";
            label2.Text = "Cb";
            label3.Text = "Cr";
            Separate(pictureBox2, c =>
            {
                var l = (int) (c.R*0.299 + c.G*0.587 + c.B*0.114);
                return Color.FromArgb(l, l, l);
            });
            Separate(pictureBox3, c =>
            {
                var Y = (c.R*0.299 + c.G*0.587 + c.B*0.114);
                var l = (int) ((c.B - Y)/1.772 + 128);
                return Color.FromArgb(128, 128, l);
            });
            Separate(pictureBox4, c =>
            {
                var Y = (c.R*0.299 + c.G*0.587 + c.B*0.114);
                var l = (int) ((c.R - Y)/1.402 + 128);
                return Color.FromArgb(l, 128, 128);
            });
        }

        private double max_b = -1000, min_b = 1000;

        Vector<double> getWhite()
        {
            return new DenseVector(new double[] {GetVal(tbWX), GetVal(tbWY), GetVal(tbWZ)});
        }

        private void Lab()
        {
            label1.Text = "L";
            label2.Text = "a";
            label3.Text = "b";
            var wh = getWhite();
            var M = Graphics2.RgbToXyz.Convert(wh, GetVal(tbRX), GetVal(tbRY), GetVal(tbGX), GetVal(tbGY),
                GetVal(tbBX), GetVal(tbBY));
            Separate(pictureBox2, c =>
            {
                var d = XyzToLab(c, M, wh);
                var L = (int) d[0];
                return Color.FromArgb(L*255/100, L*255/100, L*255/100);
            });
            Separate(pictureBox3, c =>
            {
                var d = XyzToLab(c, M, wh);
                var a = ((int) d[1] + 120);
                a = Math.Min(a, 255);
                a = Math.Max(a, 0);
                return Color.FromArgb(255-a, a, 255-a);
            });
            Separate(pictureBox4, c =>
            {
                var d = XyzToLab(c, M, wh);
                max_b = Math.Max(d[2], max_b);
                min_b = Math.Min(d[2], min_b);
                var a = ((int) d[2] + 120);
                a = Math.Min(a, 255);
                a = Math.Max(a, 0);
                return Color.FromArgb(a, a, 255-a);
            });
        }

        private void HSV()
        {
            label1.Text = "H";
            label2.Text = "S";
            label3.Text = "V";
            Separate(pictureBox2, c => RgbFromHue(getH(c)));
            Separate(pictureBox3, c => Color.FromArgb(255, 255 - getS(c), 255 - getS(c)));
            Separate(pictureBox4, c => Color.FromArgb(new[] {c.R, c.B, c.G}.Max(), 0, 0));
        }

        private void RGB()
        {
            label1.Text = "R";
            label2.Text = "G";
            label3.Text = "B";
            Separate(pictureBox2, color => Color.FromArgb(color.R, 0, 0));
            Separate(pictureBox3, color => Color.FromArgb(0, color.G, 0));
            Separate(pictureBox4, color => Color.FromArgb(0, 0, color.B));
        }

        void Separate(PictureBox pb, Func<Color, Color> f)
        {
            if (OriginalImage == null)
                return;
            if (pb.Image!=null)
                pb.Image.Dispose();
            Bitmap b = new Bitmap(pictureBox1.Image);
            Bitmap t = new Bitmap(pictureBox1.Image);
            for (int i = 0; i < t.Width; i++)
            {
                for (int j = 0; j < t.Height; j++)
                {
                    var c = t.GetPixel(i, j);
                    b.SetPixel(i, j, f(c));
                }
            }
            t.Dispose();
            pb.Image = b;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            GetColorSpace();
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            var s = comboBox2.SelectedIndex;
            fillProfiles(s);
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }
    }
}