﻿using System.Collections.Generic;

namespace Graphics2
{
    public partial class Form1
    {

        private List<double[]> Profiles = new List<double[]>()
        {
            new []{0.64,0.33,0.03,0.3,0.6,0.1,0.15,0.06,0.79,0.3127,0.3290,0.3583} ,
            new []{0.64,0.33,0.03,0.21,0.71,0.08,0.15,0.06,0.79,0.3127,0.3290,0.3583} ,
            new []{0.625,0.34,0.035,0.28,0.595,0.125,0.1550,0.07,0.7750,0.3127,0.3290,0.3583},
            new []{0.735,0.265,0,0.274,0.717,0.009,0.167,0.009,0.8240,0.3333,0.3333,0.3333} ,
            new []{0.7347,0.2653,0,0.1152,0.8264,0.0177,0.1566,0.0177,0.8257,0.3457,0.3585,0.2958} ,
            new []{0.64,0.33,0.03,0.29,0.6,0.11,0.15,0.06,0.79,0.3127,0.3290,0.3583} 
        };

        void fillProfiles(int key)
        {
            var t = new []{tbRX, tbRY, tbRZ, tbGX, tbGY, tbGZ, tbBX, tbBY, tbBZ, tbWX, tbWY, tbWZ};
            for (int i = 0; i < t.Length; i++)
            {
                t[i].Text = Profiles[key][i].ToString();
            }
        }
    }
}