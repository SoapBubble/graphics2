﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics.LinearAlgebra.Double;

namespace Graphics2
{
    class RgbToXyz
    {
        public static Matrix<double> Convert( Vector<double> white, 
            double xr, double yr, double xg, double yg, double xb, double yb)
        {
            double Xr = xr/yr,
                Yr = 1,
                Zr = (1 - xr - yr)/yr,
                Xg = xg/yg,
                Yg = 1,
                Zg = (1 - xg - yg)/yg,
                Xb = xb/yb,
                Yb = 1,
                Zb = (1 - xb - yb)/yb;
            Matrix<double> M = new DenseMatrix(3,3,new []{Xr,Yr,Zr,Xg,Yg,Zg,Xb,Yb,Zb});
            Vector<double> S = M.Inverse()*white;
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    M[i, j] *= S[j];
                }
            }
            return M;
        }
    }
}
