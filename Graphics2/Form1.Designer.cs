﻿namespace Graphics2
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.loadImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tbRX = new System.Windows.Forms.TextBox();
            this.tbGX = new System.Windows.Forms.TextBox();
            this.tbBX = new System.Windows.Forms.TextBox();
            this.tbWX = new System.Windows.Forms.TextBox();
            this.tbWY = new System.Windows.Forms.TextBox();
            this.tbBY = new System.Windows.Forms.TextBox();
            this.tbGY = new System.Windows.Forms.TextBox();
            this.tbRY = new System.Windows.Forms.TextBox();
            this.tbWZ = new System.Windows.Forms.TextBox();
            this.tbBZ = new System.Windows.Forms.TextBox();
            this.tbGZ = new System.Windows.Forms.TextBox();
            this.tbRZ = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.comboBox2 = new System.Windows.Forms.ComboBox();
            this.label12 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.loadImageToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1493, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // loadImageToolStripMenuItem
            // 
            this.loadImageToolStripMenuItem.Name = "loadImageToolStripMenuItem";
            this.loadImageToolStripMenuItem.Size = new System.Drawing.Size(100, 24);
            this.loadImageToolStripMenuItem.Text = "Load Image";
            this.loadImageToolStripMenuItem.Click += new System.EventHandler(this.loadImageToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(209, 365);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(25, 25);
            this.label1.TabIndex = 5;
            this.label1.Text = "R";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(698, 365);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 25);
            this.label2.TabIndex = 6;
            this.label2.Text = "G";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(1237, 365);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 25);
            this.label3.TabIndex = 7;
            this.label3.Text = "B";
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "RGB",
            "YCbCr",
            "HSV",
            "Lab"});
            this.comboBox1.Location = new System.Drawing.Point(481, 63);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(121, 24);
            this.comboBox1.TabIndex = 8;
            this.comboBox1.Text = "RGB";
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(481, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 17);
            this.label4.TabIndex = 9;
            this.label4.Text = "Color model";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.White;
            this.pictureBox1.Location = new System.Drawing.Point(12, 40);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(448, 311);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            // 
            // pictureBox2
            // 
            this.pictureBox2.BackColor = System.Drawing.Color.White;
            this.pictureBox2.Location = new System.Drawing.Point(12, 393);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(448, 311);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 11;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox3
            // 
            this.pictureBox3.BackColor = System.Drawing.Color.White;
            this.pictureBox3.Location = new System.Drawing.Point(509, 393);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(448, 311);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 12;
            this.pictureBox3.TabStop = false;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.White;
            this.pictureBox4.Location = new System.Drawing.Point(1014, 393);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(448, 311);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 13;
            this.pictureBox4.TabStop = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(481, 121);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(18, 17);
            this.label5.TabIndex = 14;
            this.label5.Text = "R";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(481, 161);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(19, 17);
            this.label6.TabIndex = 15;
            this.label6.Text = "G";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(481, 201);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(17, 17);
            this.label7.TabIndex = 16;
            this.label7.Text = "B";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(481, 239);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 17);
            this.label8.TabIndex = 17;
            this.label8.Text = "White";
            // 
            // tbRX
            // 
            this.tbRX.Location = new System.Drawing.Point(544, 118);
            this.tbRX.Name = "tbRX";
            this.tbRX.Size = new System.Drawing.Size(52, 22);
            this.tbRX.TabIndex = 18;
            this.tbRX.Text = "0.6400";
            // 
            // tbGX
            // 
            this.tbGX.Location = new System.Drawing.Point(544, 161);
            this.tbGX.Name = "tbGX";
            this.tbGX.Size = new System.Drawing.Size(52, 22);
            this.tbGX.TabIndex = 19;
            this.tbGX.Text = "0.3000";
            // 
            // tbBX
            // 
            this.tbBX.Location = new System.Drawing.Point(544, 198);
            this.tbBX.Name = "tbBX";
            this.tbBX.Size = new System.Drawing.Size(52, 22);
            this.tbBX.TabIndex = 20;
            this.tbBX.Text = "0.1500";
            // 
            // tbWX
            // 
            this.tbWX.Location = new System.Drawing.Point(544, 236);
            this.tbWX.Name = "tbWX";
            this.tbWX.Size = new System.Drawing.Size(52, 22);
            this.tbWX.TabIndex = 21;
            this.tbWX.Text = "0.3127";
            // 
            // tbWY
            // 
            this.tbWY.Location = new System.Drawing.Point(612, 236);
            this.tbWY.Name = "tbWY";
            this.tbWY.Size = new System.Drawing.Size(52, 22);
            this.tbWY.TabIndex = 25;
            this.tbWY.Text = "0.3290";
            // 
            // tbBY
            // 
            this.tbBY.Location = new System.Drawing.Point(612, 198);
            this.tbBY.Name = "tbBY";
            this.tbBY.Size = new System.Drawing.Size(52, 22);
            this.tbBY.TabIndex = 24;
            this.tbBY.Text = "0.0600";
            // 
            // tbGY
            // 
            this.tbGY.Location = new System.Drawing.Point(612, 161);
            this.tbGY.Name = "tbGY";
            this.tbGY.Size = new System.Drawing.Size(52, 22);
            this.tbGY.TabIndex = 23;
            this.tbGY.Text = "0.6000";
            // 
            // tbRY
            // 
            this.tbRY.Location = new System.Drawing.Point(612, 118);
            this.tbRY.Name = "tbRY";
            this.tbRY.Size = new System.Drawing.Size(52, 22);
            this.tbRY.TabIndex = 22;
            this.tbRY.Text = "0.3300";
            // 
            // tbWZ
            // 
            this.tbWZ.Location = new System.Drawing.Point(683, 236);
            this.tbWZ.Name = "tbWZ";
            this.tbWZ.Size = new System.Drawing.Size(52, 22);
            this.tbWZ.TabIndex = 29;
            this.tbWZ.Text = "1.000";
            // 
            // tbBZ
            // 
            this.tbBZ.Location = new System.Drawing.Point(683, 198);
            this.tbBZ.Name = "tbBZ";
            this.tbBZ.Size = new System.Drawing.Size(52, 22);
            this.tbBZ.TabIndex = 28;
            this.tbBZ.Text = "0.0722";
            // 
            // tbGZ
            // 
            this.tbGZ.Location = new System.Drawing.Point(683, 161);
            this.tbGZ.Name = "tbGZ";
            this.tbGZ.Size = new System.Drawing.Size(52, 22);
            this.tbGZ.TabIndex = 27;
            this.tbGZ.Text = "0.7152";
            // 
            // tbRZ
            // 
            this.tbRZ.Location = new System.Drawing.Point(683, 118);
            this.tbRZ.Name = "tbRZ";
            this.tbRZ.Size = new System.Drawing.Size(52, 22);
            this.tbRZ.TabIndex = 26;
            this.tbRZ.Text = "0.2126";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(556, 94);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(17, 17);
            this.label9.TabIndex = 30;
            this.label9.Text = "X";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(621, 93);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(17, 17);
            this.label10.TabIndex = 31;
            this.label10.Text = "Y";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(694, 93);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(17, 17);
            this.label11.TabIndex = 32;
            this.label11.Text = "Z";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(484, 282);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(118, 55);
            this.button1.TabIndex = 33;
            this.button1.Text = "Separate";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // comboBox2
            // 
            this.comboBox2.FormattingEnabled = true;
            this.comboBox2.Items.AddRange(new object[] {
            "sRGB",
            "Adobe RGB",
            "Apple RGB",
            "CIE RGB",
            "Wide Gamut",
            "PAL/SECAM"});
            this.comboBox2.Location = new System.Drawing.Point(683, 63);
            this.comboBox2.Name = "comboBox2";
            this.comboBox2.Size = new System.Drawing.Size(121, 24);
            this.comboBox2.TabIndex = 34;
            this.comboBox2.Text = "sRGB";
            this.comboBox2.SelectedIndexChanged += new System.EventHandler(this.comboBox2_SelectedIndexChanged);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(683, 40);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(84, 17);
            this.label12.TabIndex = 35;
            this.label12.Text = "Color profile";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1493, 741);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.comboBox2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.tbWZ);
            this.Controls.Add(this.tbBZ);
            this.Controls.Add(this.tbGZ);
            this.Controls.Add(this.tbRZ);
            this.Controls.Add(this.tbWY);
            this.Controls.Add(this.tbBY);
            this.Controls.Add(this.tbGY);
            this.Controls.Add(this.tbRY);
            this.Controls.Add(this.tbWX);
            this.Controls.Add(this.tbBX);
            this.Controls.Add(this.tbGX);
            this.Controls.Add(this.tbRX);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.pictureBox4);
            this.Controls.Add(this.pictureBox3);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem loadImageToolStripMenuItem;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox tbRX;
        private System.Windows.Forms.TextBox tbGX;
        private System.Windows.Forms.TextBox tbBX;
        private System.Windows.Forms.TextBox tbWX;
        private System.Windows.Forms.TextBox tbWY;
        private System.Windows.Forms.TextBox tbBY;
        private System.Windows.Forms.TextBox tbGY;
        private System.Windows.Forms.TextBox tbRY;
        private System.Windows.Forms.TextBox tbWZ;
        private System.Windows.Forms.TextBox tbBZ;
        private System.Windows.Forms.TextBox tbGZ;
        private System.Windows.Forms.TextBox tbRZ;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.ComboBox comboBox2;
        private System.Windows.Forms.Label label12;
    }
}

